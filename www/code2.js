gdjs.HighScoresCode = {};
gdjs.HighScoresCode.GDGrassObjects1= [];
gdjs.HighScoresCode.GDGrassObjects2= [];
gdjs.HighScoresCode.GDGrassObjects3= [];
gdjs.HighScoresCode.GDTreesObjects1= [];
gdjs.HighScoresCode.GDTreesObjects2= [];
gdjs.HighScoresCode.GDTreesObjects3= [];
gdjs.HighScoresCode.GDSkyObjects1= [];
gdjs.HighScoresCode.GDSkyObjects2= [];
gdjs.HighScoresCode.GDSkyObjects3= [];
gdjs.HighScoresCode.GDCloudsObjects1= [];
gdjs.HighScoresCode.GDCloudsObjects2= [];
gdjs.HighScoresCode.GDCloudsObjects3= [];
gdjs.HighScoresCode.GDBirdObjects1= [];
gdjs.HighScoresCode.GDBirdObjects2= [];
gdjs.HighScoresCode.GDBirdObjects3= [];
gdjs.HighScoresCode.GDDogMissedObjects1= [];
gdjs.HighScoresCode.GDDogMissedObjects2= [];
gdjs.HighScoresCode.GDDogMissedObjects3= [];
gdjs.HighScoresCode.GDKnuckHuntObjects1= [];
gdjs.HighScoresCode.GDKnuckHuntObjects2= [];
gdjs.HighScoresCode.GDKnuckHuntObjects3= [];
gdjs.HighScoresCode.GDBackObjects1= [];
gdjs.HighScoresCode.GDBackObjects2= [];
gdjs.HighScoresCode.GDBackObjects3= [];
gdjs.HighScoresCode.GDBoxObjects1= [];
gdjs.HighScoresCode.GDBoxObjects2= [];
gdjs.HighScoresCode.GDBoxObjects3= [];
gdjs.HighScoresCode.GDHighScoreTextObjects1= [];
gdjs.HighScoresCode.GDHighScoreTextObjects2= [];
gdjs.HighScoresCode.GDHighScoreTextObjects3= [];
gdjs.HighScoresCode.GDChangeNameObjects1= [];
gdjs.HighScoresCode.GDChangeNameObjects2= [];
gdjs.HighScoresCode.GDChangeNameObjects3= [];

gdjs.HighScoresCode.conditionTrue_0 = {val:false};
gdjs.HighScoresCode.condition0IsTrue_0 = {val:false};
gdjs.HighScoresCode.condition1IsTrue_0 = {val:false};
gdjs.HighScoresCode.condition2IsTrue_0 = {val:false};


gdjs.HighScoresCode.eventsList0x6b8954 = function(runtimeScene) {

{


gdjs.HighScoresCode.condition0IsTrue_0.val = false;
{
gdjs.HighScoresCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("ResponseTopScores")) == "\"0\"";
}if (gdjs.HighScoresCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("ResponseTopScores").setString("NO CONNECTION");
}}

}


{


{
gdjs.HighScoresCode.GDHighScoreTextObjects1.createFrom(runtimeScene.getObjects("HighScoreText"));
{for(var i = 0, len = gdjs.HighScoresCode.GDHighScoreTextObjects1.length ;i < len;++i) {
    gdjs.HighScoresCode.GDHighScoreTextObjects1[i].setBBText(gdjs.HighScoresCode.GDHighScoreTextObjects1[i].getBBText() + (gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("ResponseTopScores"))));
}
}}

}


}; //End of gdjs.HighScoresCode.eventsList0x6b8954
gdjs.HighScoresCode.eventsList0x6b8d14 = function(runtimeScene) {

{


{
{}}

}


{


{
{gdjs.evtTools.network.sendHttpRequest("https://hacktomatest.com", "/TopScoresKnuckHunt.php", "", "GET", "", runtimeScene.getVariables().get("ResponseTopScores"));
}
{ //Subevents
gdjs.HighScoresCode.eventsList0x6b8954(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.HighScoresCode.eventsList0x6b8d14
gdjs.HighScoresCode.mapOfGDgdjs_46HighScoresCode_46GDBackObjects1Objects = Hashtable.newFrom({"Back": gdjs.HighScoresCode.GDBackObjects1});gdjs.HighScoresCode.mapOfGDgdjs_46HighScoresCode_46GDChangeNameObjects1Objects = Hashtable.newFrom({"ChangeName": gdjs.HighScoresCode.GDChangeNameObjects1});gdjs.HighScoresCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.HighScoresCode.condition0IsTrue_0.val = false;
{
gdjs.HighScoresCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.HighScoresCode.condition0IsTrue_0.val) {
{gdjs.adMob.showInterstitial();
}
{ //Subevents
gdjs.HighScoresCode.eventsList0x6b8d14(runtimeScene);} //End of subevents
}

}


{


gdjs.HighScoresCode.condition0IsTrue_0.val = false;
{
gdjs.HighScoresCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.HighScoresCode.condition0IsTrue_0.val) {
gdjs.HighScoresCode.GDBoxObjects1.createFrom(runtimeScene.getObjects("Box"));
{for(var i = 0, len = gdjs.HighScoresCode.GDBoxObjects1.length ;i < len;++i) {
    gdjs.HighScoresCode.GDBoxObjects1[i].setOpacity(222);
}
}}

}


{


{
gdjs.HighScoresCode.GDCloudsObjects1.createFrom(runtimeScene.getObjects("Clouds"));
gdjs.HighScoresCode.GDGrassObjects1.createFrom(runtimeScene.getObjects("Grass"));
gdjs.HighScoresCode.GDTreesObjects1.createFrom(runtimeScene.getObjects("Trees"));
{for(var i = 0, len = gdjs.HighScoresCode.GDCloudsObjects1.length ;i < len;++i) {
    gdjs.HighScoresCode.GDCloudsObjects1[i].setXOffset(gdjs.HighScoresCode.GDCloudsObjects1[i].getXOffset() + (0.15));
}
}{for(var i = 0, len = gdjs.HighScoresCode.GDTreesObjects1.length ;i < len;++i) {
    gdjs.HighScoresCode.GDTreesObjects1[i].setXOffset(gdjs.HighScoresCode.GDTreesObjects1[i].getXOffset() + (0.04));
}
}{for(var i = 0, len = gdjs.HighScoresCode.GDGrassObjects1.length ;i < len;++i) {
    gdjs.HighScoresCode.GDGrassObjects1[i].setXOffset(gdjs.HighScoresCode.GDGrassObjects1[i].getXOffset() + (0.05));
}
}}

}


{

gdjs.HighScoresCode.GDBackObjects1.createFrom(runtimeScene.getObjects("Back"));

gdjs.HighScoresCode.condition0IsTrue_0.val = false;
gdjs.HighScoresCode.condition1IsTrue_0.val = false;
{
gdjs.HighScoresCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.HighScoresCode.condition0IsTrue_0.val ) {
{
gdjs.HighScoresCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.HighScoresCode.mapOfGDgdjs_46HighScoresCode_46GDBackObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.HighScoresCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Title", false);
}}

}


{

gdjs.HighScoresCode.GDChangeNameObjects1.createFrom(runtimeScene.getObjects("ChangeName"));

gdjs.HighScoresCode.condition0IsTrue_0.val = false;
gdjs.HighScoresCode.condition1IsTrue_0.val = false;
{
gdjs.HighScoresCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.HighScoresCode.condition0IsTrue_0.val ) {
{
gdjs.HighScoresCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.HighScoresCode.mapOfGDgdjs_46HighScoresCode_46GDChangeNameObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.HighScoresCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Settings", false);
}}

}


}; //End of gdjs.HighScoresCode.eventsList0x5b7028


gdjs.HighScoresCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.HighScoresCode.GDGrassObjects1.length = 0;
gdjs.HighScoresCode.GDGrassObjects2.length = 0;
gdjs.HighScoresCode.GDGrassObjects3.length = 0;
gdjs.HighScoresCode.GDTreesObjects1.length = 0;
gdjs.HighScoresCode.GDTreesObjects2.length = 0;
gdjs.HighScoresCode.GDTreesObjects3.length = 0;
gdjs.HighScoresCode.GDSkyObjects1.length = 0;
gdjs.HighScoresCode.GDSkyObjects2.length = 0;
gdjs.HighScoresCode.GDSkyObjects3.length = 0;
gdjs.HighScoresCode.GDCloudsObjects1.length = 0;
gdjs.HighScoresCode.GDCloudsObjects2.length = 0;
gdjs.HighScoresCode.GDCloudsObjects3.length = 0;
gdjs.HighScoresCode.GDBirdObjects1.length = 0;
gdjs.HighScoresCode.GDBirdObjects2.length = 0;
gdjs.HighScoresCode.GDBirdObjects3.length = 0;
gdjs.HighScoresCode.GDDogMissedObjects1.length = 0;
gdjs.HighScoresCode.GDDogMissedObjects2.length = 0;
gdjs.HighScoresCode.GDDogMissedObjects3.length = 0;
gdjs.HighScoresCode.GDKnuckHuntObjects1.length = 0;
gdjs.HighScoresCode.GDKnuckHuntObjects2.length = 0;
gdjs.HighScoresCode.GDKnuckHuntObjects3.length = 0;
gdjs.HighScoresCode.GDBackObjects1.length = 0;
gdjs.HighScoresCode.GDBackObjects2.length = 0;
gdjs.HighScoresCode.GDBackObjects3.length = 0;
gdjs.HighScoresCode.GDBoxObjects1.length = 0;
gdjs.HighScoresCode.GDBoxObjects2.length = 0;
gdjs.HighScoresCode.GDBoxObjects3.length = 0;
gdjs.HighScoresCode.GDHighScoreTextObjects1.length = 0;
gdjs.HighScoresCode.GDHighScoreTextObjects2.length = 0;
gdjs.HighScoresCode.GDHighScoreTextObjects3.length = 0;
gdjs.HighScoresCode.GDChangeNameObjects1.length = 0;
gdjs.HighScoresCode.GDChangeNameObjects2.length = 0;
gdjs.HighScoresCode.GDChangeNameObjects3.length = 0;

gdjs.HighScoresCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['HighScoresCode'] = gdjs.HighScoresCode;
