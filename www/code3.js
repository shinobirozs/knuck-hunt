gdjs.SettingsCode = {};
gdjs.SettingsCode.GDSkyObjects1= [];
gdjs.SettingsCode.GDSkyObjects2= [];
gdjs.SettingsCode.GDSkyObjects3= [];
gdjs.SettingsCode.GDGrassObjects1= [];
gdjs.SettingsCode.GDGrassObjects2= [];
gdjs.SettingsCode.GDGrassObjects3= [];
gdjs.SettingsCode.GDKeyboardCaseObjects1= [];
gdjs.SettingsCode.GDKeyboardCaseObjects2= [];
gdjs.SettingsCode.GDKeyboardCaseObjects3= [];
gdjs.SettingsCode.GDKeysJibiObjects1= [];
gdjs.SettingsCode.GDKeysJibiObjects2= [];
gdjs.SettingsCode.GDKeysJibiObjects3= [];
gdjs.SettingsCode.GDSubmitNameObjects1= [];
gdjs.SettingsCode.GDSubmitNameObjects2= [];
gdjs.SettingsCode.GDSubmitNameObjects3= [];
gdjs.SettingsCode.GDAgreementObjects1= [];
gdjs.SettingsCode.GDAgreementObjects2= [];
gdjs.SettingsCode.GDAgreementObjects3= [];
gdjs.SettingsCode.GDClearNameObjects1= [];
gdjs.SettingsCode.GDClearNameObjects2= [];
gdjs.SettingsCode.GDClearNameObjects3= [];
gdjs.SettingsCode.GDPlayerNameTextObjects1= [];
gdjs.SettingsCode.GDPlayerNameTextObjects2= [];
gdjs.SettingsCode.GDPlayerNameTextObjects3= [];
gdjs.SettingsCode.GDBirdObjects1= [];
gdjs.SettingsCode.GDBirdObjects2= [];
gdjs.SettingsCode.GDBirdObjects3= [];
gdjs.SettingsCode.GDDogMissedObjects1= [];
gdjs.SettingsCode.GDDogMissedObjects2= [];
gdjs.SettingsCode.GDDogMissedObjects3= [];
gdjs.SettingsCode.GDCloudsObjects1= [];
gdjs.SettingsCode.GDCloudsObjects2= [];
gdjs.SettingsCode.GDCloudsObjects3= [];
gdjs.SettingsCode.GDDialogObjects1= [];
gdjs.SettingsCode.GDDialogObjects2= [];
gdjs.SettingsCode.GDDialogObjects3= [];

gdjs.SettingsCode.conditionTrue_0 = {val:false};
gdjs.SettingsCode.condition0IsTrue_0 = {val:false};
gdjs.SettingsCode.condition1IsTrue_0 = {val:false};
gdjs.SettingsCode.condition2IsTrue_0 = {val:false};
gdjs.SettingsCode.condition3IsTrue_0 = {val:false};
gdjs.SettingsCode.conditionTrue_1 = {val:false};
gdjs.SettingsCode.condition0IsTrue_1 = {val:false};
gdjs.SettingsCode.condition1IsTrue_1 = {val:false};
gdjs.SettingsCode.condition2IsTrue_1 = {val:false};
gdjs.SettingsCode.condition3IsTrue_1 = {val:false};


gdjs.SettingsCode.eventsList0x6b8d14 = function(runtimeScene) {

{


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")) == "0";
}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("StoragePlayerName").setString("HUNTER");
}}

}


{


{
gdjs.SettingsCode.GDPlayerNameTextObjects1.createFrom(runtimeScene.getObjects("PlayerNameText"));
{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects1.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects1[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")));
}
}}

}


}; //End of gdjs.SettingsCode.eventsList0x6b8d14
gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDKeysJibiObjects1Objects = Hashtable.newFrom({"KeysJibi": gdjs.SettingsCode.GDKeysJibiObjects1});gdjs.SettingsCode.eventsList0x6bb15c = function(runtimeScene) {

{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 0 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("A"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 1 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("B"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 2 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("C"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 3 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("D"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 4 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("E"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 5 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("F"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 6 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("G"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 7 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("H"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 8 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("I"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 9 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("J"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 10 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("K"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 11 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("L"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 12 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("M"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 13 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("N"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 14 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("O"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 15 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("P"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 16 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("Q"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 17 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("R"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 18 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("S"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 19 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("T"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 20 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("U"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 21 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("V"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 22 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("W"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 23 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("X"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 24 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("Y"));
}
}}

}


{

gdjs.SettingsCode.GDKeysJibiObjects2.createFrom(gdjs.SettingsCode.GDKeysJibiObjects1);


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects2.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects2[i].getAnimation() == 25 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects2[k] = gdjs.SettingsCode.GDKeysJibiObjects2[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects2.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects2.createFrom(gdjs.SettingsCode.GDPlayerNameTextObjects1);

{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects2.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects2[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects2[i].getString() + ("Z"));
}
}}

}


{

/* Reuse gdjs.SettingsCode.GDKeysJibiObjects1 */

gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.SettingsCode.GDKeysJibiObjects1.length;i<l;++i) {
    if ( gdjs.SettingsCode.GDKeysJibiObjects1[i].getAnimation() == 26 ) {
        gdjs.SettingsCode.condition0IsTrue_0.val = true;
        gdjs.SettingsCode.GDKeysJibiObjects1[k] = gdjs.SettingsCode.GDKeysJibiObjects1[i];
        ++k;
    }
}
gdjs.SettingsCode.GDKeysJibiObjects1.length = k;}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SettingsCode.GDPlayerNameTextObjects1 */
{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects1.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects1[i].setString(gdjs.SettingsCode.GDPlayerNameTextObjects1[i].getString() + ("."));
}
}}

}


}; //End of gdjs.SettingsCode.eventsList0x6bb15c
gdjs.SettingsCode.eventsList0x6bb6dc = function(runtimeScene) {

{


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("PlayerNameCount")) < 10;
}if (gdjs.SettingsCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.SettingsCode.eventsList0x6bb15c(runtimeScene);} //End of subevents
}

}


}; //End of gdjs.SettingsCode.eventsList0x6bb6dc
gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDClearNameObjects1Objects = Hashtable.newFrom({"ClearName": gdjs.SettingsCode.GDClearNameObjects1});gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDSubmitNameObjects1Objects = Hashtable.newFrom({"SubmitName": gdjs.SettingsCode.GDSubmitNameObjects1});gdjs.SettingsCode.eventsList0x6bcddc = function(runtimeScene) {

{


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("PlayerNameCount")) > 0;
}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
/* Reuse gdjs.SettingsCode.GDPlayerNameTextObjects1 */
{gdjs.evtTools.storage.writeStringInJSONFile("KnuckHunt", "PlayerName", (( gdjs.SettingsCode.GDPlayerNameTextObjects1.length === 0 ) ? "" :gdjs.SettingsCode.GDPlayerNameTextObjects1[0].getString()));
}{runtimeScene.getVariables().get("StoragePlayerName").setString((( gdjs.SettingsCode.GDPlayerNameTextObjects1.length === 0 ) ? "" :gdjs.SettingsCode.GDPlayerNameTextObjects1[0].getString()));
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "HighScores", true);
}}

}


}; //End of gdjs.SettingsCode.eventsList0x6bcddc
gdjs.SettingsCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SettingsCode.condition0IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SettingsCode.condition0IsTrue_0.val) {
{gdjs.evtTools.storage.readStringFromJSONFile("KnuckHunt", "PlayerName", runtimeScene, runtimeScene.getVariables().get("StoragePlayerName"));
}
{ //Subevents
gdjs.SettingsCode.eventsList0x6b8d14(runtimeScene);} //End of subevents
}

}


{

gdjs.SettingsCode.GDKeysJibiObjects1.createFrom(runtimeScene.getObjects("KeysJibi"));

gdjs.SettingsCode.condition0IsTrue_0.val = false;
gdjs.SettingsCode.condition1IsTrue_0.val = false;
gdjs.SettingsCode.condition2IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SettingsCode.condition0IsTrue_0.val ) {
{
gdjs.SettingsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDKeysJibiObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SettingsCode.condition1IsTrue_0.val ) {
{
{gdjs.SettingsCode.conditionTrue_1 = gdjs.SettingsCode.condition2IsTrue_0;
gdjs.SettingsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7037124);
}
}}
}
if (gdjs.SettingsCode.condition2IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects1.createFrom(runtimeScene.getObjects("PlayerNameText"));
{runtimeScene.getVariables().get("PlayerNameCount").setNumber(gdjs.evtTools.string.strLen((( gdjs.SettingsCode.GDPlayerNameTextObjects1.length === 0 ) ? "" :gdjs.SettingsCode.GDPlayerNameTextObjects1[0].getString())));
}
{ //Subevents
gdjs.SettingsCode.eventsList0x6bb6dc(runtimeScene);} //End of subevents
}

}


{

gdjs.SettingsCode.GDClearNameObjects1.createFrom(runtimeScene.getObjects("ClearName"));

gdjs.SettingsCode.condition0IsTrue_0.val = false;
gdjs.SettingsCode.condition1IsTrue_0.val = false;
gdjs.SettingsCode.condition2IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SettingsCode.condition0IsTrue_0.val ) {
{
gdjs.SettingsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDClearNameObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SettingsCode.condition1IsTrue_0.val ) {
{
{gdjs.SettingsCode.conditionTrue_1 = gdjs.SettingsCode.condition2IsTrue_0;
gdjs.SettingsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7064620);
}
}}
}
if (gdjs.SettingsCode.condition2IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects1.createFrom(runtimeScene.getObjects("PlayerNameText"));
{for(var i = 0, len = gdjs.SettingsCode.GDPlayerNameTextObjects1.length ;i < len;++i) {
    gdjs.SettingsCode.GDPlayerNameTextObjects1[i].setString("");
}
}}

}


{

gdjs.SettingsCode.GDSubmitNameObjects1.createFrom(runtimeScene.getObjects("SubmitName"));

gdjs.SettingsCode.condition0IsTrue_0.val = false;
gdjs.SettingsCode.condition1IsTrue_0.val = false;
gdjs.SettingsCode.condition2IsTrue_0.val = false;
{
gdjs.SettingsCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.SettingsCode.condition0IsTrue_0.val ) {
{
gdjs.SettingsCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SettingsCode.mapOfGDgdjs_46SettingsCode_46GDSubmitNameObjects1Objects, runtimeScene, true, false);
}if ( gdjs.SettingsCode.condition1IsTrue_0.val ) {
{
{gdjs.SettingsCode.conditionTrue_1 = gdjs.SettingsCode.condition2IsTrue_0;
gdjs.SettingsCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7065452);
}
}}
}
if (gdjs.SettingsCode.condition2IsTrue_0.val) {
gdjs.SettingsCode.GDPlayerNameTextObjects1.createFrom(runtimeScene.getObjects("PlayerNameText"));
{runtimeScene.getVariables().get("PlayerNameCount").setNumber(gdjs.evtTools.string.strLen((( gdjs.SettingsCode.GDPlayerNameTextObjects1.length === 0 ) ? "" :gdjs.SettingsCode.GDPlayerNameTextObjects1[0].getString())));
}
{ //Subevents
gdjs.SettingsCode.eventsList0x6bcddc(runtimeScene);} //End of subevents
}

}


{


{
gdjs.SettingsCode.GDCloudsObjects1.createFrom(runtimeScene.getObjects("Clouds"));
{for(var i = 0, len = gdjs.SettingsCode.GDCloudsObjects1.length ;i < len;++i) {
    gdjs.SettingsCode.GDCloudsObjects1[i].setXOffset(gdjs.SettingsCode.GDCloudsObjects1[i].getXOffset() + (0.15));
}
}}

}


}; //End of gdjs.SettingsCode.eventsList0x5b7028


gdjs.SettingsCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SettingsCode.GDSkyObjects1.length = 0;
gdjs.SettingsCode.GDSkyObjects2.length = 0;
gdjs.SettingsCode.GDSkyObjects3.length = 0;
gdjs.SettingsCode.GDGrassObjects1.length = 0;
gdjs.SettingsCode.GDGrassObjects2.length = 0;
gdjs.SettingsCode.GDGrassObjects3.length = 0;
gdjs.SettingsCode.GDKeyboardCaseObjects1.length = 0;
gdjs.SettingsCode.GDKeyboardCaseObjects2.length = 0;
gdjs.SettingsCode.GDKeyboardCaseObjects3.length = 0;
gdjs.SettingsCode.GDKeysJibiObjects1.length = 0;
gdjs.SettingsCode.GDKeysJibiObjects2.length = 0;
gdjs.SettingsCode.GDKeysJibiObjects3.length = 0;
gdjs.SettingsCode.GDSubmitNameObjects1.length = 0;
gdjs.SettingsCode.GDSubmitNameObjects2.length = 0;
gdjs.SettingsCode.GDSubmitNameObjects3.length = 0;
gdjs.SettingsCode.GDAgreementObjects1.length = 0;
gdjs.SettingsCode.GDAgreementObjects2.length = 0;
gdjs.SettingsCode.GDAgreementObjects3.length = 0;
gdjs.SettingsCode.GDClearNameObjects1.length = 0;
gdjs.SettingsCode.GDClearNameObjects2.length = 0;
gdjs.SettingsCode.GDClearNameObjects3.length = 0;
gdjs.SettingsCode.GDPlayerNameTextObjects1.length = 0;
gdjs.SettingsCode.GDPlayerNameTextObjects2.length = 0;
gdjs.SettingsCode.GDPlayerNameTextObjects3.length = 0;
gdjs.SettingsCode.GDBirdObjects1.length = 0;
gdjs.SettingsCode.GDBirdObjects2.length = 0;
gdjs.SettingsCode.GDBirdObjects3.length = 0;
gdjs.SettingsCode.GDDogMissedObjects1.length = 0;
gdjs.SettingsCode.GDDogMissedObjects2.length = 0;
gdjs.SettingsCode.GDDogMissedObjects3.length = 0;
gdjs.SettingsCode.GDCloudsObjects1.length = 0;
gdjs.SettingsCode.GDCloudsObjects2.length = 0;
gdjs.SettingsCode.GDCloudsObjects3.length = 0;
gdjs.SettingsCode.GDDialogObjects1.length = 0;
gdjs.SettingsCode.GDDialogObjects2.length = 0;
gdjs.SettingsCode.GDDialogObjects3.length = 0;

gdjs.SettingsCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SettingsCode'] = gdjs.SettingsCode;
