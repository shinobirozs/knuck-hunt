gdjs.OtherGamesCode = {};
gdjs.OtherGamesCode.GDGrassObjects1= [];
gdjs.OtherGamesCode.GDGrassObjects2= [];
gdjs.OtherGamesCode.GDTreesObjects1= [];
gdjs.OtherGamesCode.GDTreesObjects2= [];
gdjs.OtherGamesCode.GDSkyObjects1= [];
gdjs.OtherGamesCode.GDSkyObjects2= [];
gdjs.OtherGamesCode.GDCloudsObjects1= [];
gdjs.OtherGamesCode.GDCloudsObjects2= [];
gdjs.OtherGamesCode.GDBirdObjects1= [];
gdjs.OtherGamesCode.GDBirdObjects2= [];
gdjs.OtherGamesCode.GDDogMissedObjects1= [];
gdjs.OtherGamesCode.GDDogMissedObjects2= [];
gdjs.OtherGamesCode.GDKnuckHuntObjects1= [];
gdjs.OtherGamesCode.GDKnuckHuntObjects2= [];
gdjs.OtherGamesCode.GDBackObjects1= [];
gdjs.OtherGamesCode.GDBackObjects2= [];
gdjs.OtherGamesCode.GDBoxObjects1= [];
gdjs.OtherGamesCode.GDBoxObjects2= [];
gdjs.OtherGamesCode.GDSERZObjects1= [];
gdjs.OtherGamesCode.GDSERZObjects2= [];
gdjs.OtherGamesCode.GDDBSObjects1= [];
gdjs.OtherGamesCode.GDDBSObjects2= [];
gdjs.OtherGamesCode.GDAOTObjects1= [];
gdjs.OtherGamesCode.GDAOTObjects2= [];
gdjs.OtherGamesCode.GDTOHObjects1= [];
gdjs.OtherGamesCode.GDTOHObjects2= [];
gdjs.OtherGamesCode.GDTryGamesObjects1= [];
gdjs.OtherGamesCode.GDTryGamesObjects2= [];

gdjs.OtherGamesCode.conditionTrue_0 = {val:false};
gdjs.OtherGamesCode.condition0IsTrue_0 = {val:false};
gdjs.OtherGamesCode.condition1IsTrue_0 = {val:false};
gdjs.OtherGamesCode.condition2IsTrue_0 = {val:false};


gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDBackObjects1Objects = Hashtable.newFrom({"Back": gdjs.OtherGamesCode.GDBackObjects1});gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDSERZObjects1Objects = Hashtable.newFrom({"SERZ": gdjs.OtherGamesCode.GDSERZObjects1});gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDDBSObjects1Objects = Hashtable.newFrom({"DBS": gdjs.OtherGamesCode.GDDBSObjects1});gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDAOTObjects1Objects = Hashtable.newFrom({"AOT": gdjs.OtherGamesCode.GDAOTObjects1});gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDTOHObjects1Objects = Hashtable.newFrom({"TOH": gdjs.OtherGamesCode.GDTOHObjects1});gdjs.OtherGamesCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.OtherGamesCode.condition0IsTrue_0.val) {
gdjs.OtherGamesCode.GDBoxObjects1.createFrom(runtimeScene.getObjects("Box"));
{for(var i = 0, len = gdjs.OtherGamesCode.GDBoxObjects1.length ;i < len;++i) {
    gdjs.OtherGamesCode.GDBoxObjects1[i].setOpacity(200);
}
}}

}


{


{
gdjs.OtherGamesCode.GDCloudsObjects1.createFrom(runtimeScene.getObjects("Clouds"));
gdjs.OtherGamesCode.GDGrassObjects1.createFrom(runtimeScene.getObjects("Grass"));
gdjs.OtherGamesCode.GDTreesObjects1.createFrom(runtimeScene.getObjects("Trees"));
{for(var i = 0, len = gdjs.OtherGamesCode.GDCloudsObjects1.length ;i < len;++i) {
    gdjs.OtherGamesCode.GDCloudsObjects1[i].setXOffset(gdjs.OtherGamesCode.GDCloudsObjects1[i].getXOffset() + (0.15));
}
}{for(var i = 0, len = gdjs.OtherGamesCode.GDTreesObjects1.length ;i < len;++i) {
    gdjs.OtherGamesCode.GDTreesObjects1[i].setXOffset(gdjs.OtherGamesCode.GDTreesObjects1[i].getXOffset() + (0.04));
}
}{for(var i = 0, len = gdjs.OtherGamesCode.GDGrassObjects1.length ;i < len;++i) {
    gdjs.OtherGamesCode.GDGrassObjects1[i].setXOffset(gdjs.OtherGamesCode.GDGrassObjects1[i].getXOffset() + (0.05));
}
}}

}


{

gdjs.OtherGamesCode.GDBackObjects1.createFrom(runtimeScene.getObjects("Back"));

gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
gdjs.OtherGamesCode.condition1IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.OtherGamesCode.condition0IsTrue_0.val ) {
{
gdjs.OtherGamesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDBackObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.OtherGamesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Title", false);
}}

}


{

gdjs.OtherGamesCode.GDSERZObjects1.createFrom(runtimeScene.getObjects("SERZ"));

gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
gdjs.OtherGamesCode.condition1IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.OtherGamesCode.condition0IsTrue_0.val ) {
{
gdjs.OtherGamesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDSERZObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.OtherGamesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://play.google.com/store/apps/details?id=com.residentevil.zombie", runtimeScene);
}}

}


{

gdjs.OtherGamesCode.GDDBSObjects1.createFrom(runtimeScene.getObjects("DBS"));

gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
gdjs.OtherGamesCode.condition1IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.OtherGamesCode.condition0IsTrue_0.val ) {
{
gdjs.OtherGamesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDDBSObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.OtherGamesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://play.google.com/store/apps/details?id=com.shinobirozs.deathstrandingbike", runtimeScene);
}}

}


{

gdjs.OtherGamesCode.GDAOTObjects1.createFrom(runtimeScene.getObjects("AOT"));

gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
gdjs.OtherGamesCode.condition1IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.OtherGamesCode.condition0IsTrue_0.val ) {
{
gdjs.OtherGamesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDAOTObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.OtherGamesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://play.google.com/store/apps/details?id=com.shinobirozs.attackonhuman", runtimeScene);
}}

}


{

gdjs.OtherGamesCode.GDTOHObjects1.createFrom(runtimeScene.getObjects("TOH"));

gdjs.OtherGamesCode.condition0IsTrue_0.val = false;
gdjs.OtherGamesCode.condition1IsTrue_0.val = false;
{
gdjs.OtherGamesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.OtherGamesCode.condition0IsTrue_0.val ) {
{
gdjs.OtherGamesCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.OtherGamesCode.mapOfGDgdjs_46OtherGamesCode_46GDTOHObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.OtherGamesCode.condition1IsTrue_0.val) {
{gdjs.evtTools.window.openURL("https://play.google.com/store/apps/details?id=com.roblox.towerofhell", runtimeScene);
}}

}


}; //End of gdjs.OtherGamesCode.eventsList0x5b7028


gdjs.OtherGamesCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.OtherGamesCode.GDGrassObjects1.length = 0;
gdjs.OtherGamesCode.GDGrassObjects2.length = 0;
gdjs.OtherGamesCode.GDTreesObjects1.length = 0;
gdjs.OtherGamesCode.GDTreesObjects2.length = 0;
gdjs.OtherGamesCode.GDSkyObjects1.length = 0;
gdjs.OtherGamesCode.GDSkyObjects2.length = 0;
gdjs.OtherGamesCode.GDCloudsObjects1.length = 0;
gdjs.OtherGamesCode.GDCloudsObjects2.length = 0;
gdjs.OtherGamesCode.GDBirdObjects1.length = 0;
gdjs.OtherGamesCode.GDBirdObjects2.length = 0;
gdjs.OtherGamesCode.GDDogMissedObjects1.length = 0;
gdjs.OtherGamesCode.GDDogMissedObjects2.length = 0;
gdjs.OtherGamesCode.GDKnuckHuntObjects1.length = 0;
gdjs.OtherGamesCode.GDKnuckHuntObjects2.length = 0;
gdjs.OtherGamesCode.GDBackObjects1.length = 0;
gdjs.OtherGamesCode.GDBackObjects2.length = 0;
gdjs.OtherGamesCode.GDBoxObjects1.length = 0;
gdjs.OtherGamesCode.GDBoxObjects2.length = 0;
gdjs.OtherGamesCode.GDSERZObjects1.length = 0;
gdjs.OtherGamesCode.GDSERZObjects2.length = 0;
gdjs.OtherGamesCode.GDDBSObjects1.length = 0;
gdjs.OtherGamesCode.GDDBSObjects2.length = 0;
gdjs.OtherGamesCode.GDAOTObjects1.length = 0;
gdjs.OtherGamesCode.GDAOTObjects2.length = 0;
gdjs.OtherGamesCode.GDTOHObjects1.length = 0;
gdjs.OtherGamesCode.GDTOHObjects2.length = 0;
gdjs.OtherGamesCode.GDTryGamesObjects1.length = 0;
gdjs.OtherGamesCode.GDTryGamesObjects2.length = 0;

gdjs.OtherGamesCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['OtherGamesCode'] = gdjs.OtherGamesCode;
