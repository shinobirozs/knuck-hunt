gdjs.TitleCode = {};
gdjs.TitleCode.GDGrassObjects1= [];
gdjs.TitleCode.GDGrassObjects2= [];
gdjs.TitleCode.GDGrassObjects3= [];
gdjs.TitleCode.GDTreesObjects1= [];
gdjs.TitleCode.GDTreesObjects2= [];
gdjs.TitleCode.GDTreesObjects3= [];
gdjs.TitleCode.GDSkyObjects1= [];
gdjs.TitleCode.GDSkyObjects2= [];
gdjs.TitleCode.GDSkyObjects3= [];
gdjs.TitleCode.GDCloudsObjects1= [];
gdjs.TitleCode.GDCloudsObjects2= [];
gdjs.TitleCode.GDCloudsObjects3= [];
gdjs.TitleCode.GDBirdObjects1= [];
gdjs.TitleCode.GDBirdObjects2= [];
gdjs.TitleCode.GDBirdObjects3= [];
gdjs.TitleCode.GDDogMissedObjects1= [];
gdjs.TitleCode.GDDogMissedObjects2= [];
gdjs.TitleCode.GDDogMissedObjects3= [];
gdjs.TitleCode.GDKnuckHuntObjects1= [];
gdjs.TitleCode.GDKnuckHuntObjects2= [];
gdjs.TitleCode.GDKnuckHuntObjects3= [];
gdjs.TitleCode.GDStartGameObjects1= [];
gdjs.TitleCode.GDStartGameObjects2= [];
gdjs.TitleCode.GDStartGameObjects3= [];
gdjs.TitleCode.GDHighScoresObjects1= [];
gdjs.TitleCode.GDHighScoresObjects2= [];
gdjs.TitleCode.GDHighScoresObjects3= [];
gdjs.TitleCode.GDBoxObjects1= [];
gdjs.TitleCode.GDBoxObjects2= [];
gdjs.TitleCode.GDBoxObjects3= [];
gdjs.TitleCode.GDOtherGamesObjects1= [];
gdjs.TitleCode.GDOtherGamesObjects2= [];
gdjs.TitleCode.GDOtherGamesObjects3= [];
gdjs.TitleCode.GDPlayerScoreObjects1= [];
gdjs.TitleCode.GDPlayerScoreObjects2= [];
gdjs.TitleCode.GDPlayerScoreObjects3= [];
gdjs.TitleCode.GDPlayerNameObjects1= [];
gdjs.TitleCode.GDPlayerNameObjects2= [];
gdjs.TitleCode.GDPlayerNameObjects3= [];

gdjs.TitleCode.conditionTrue_0 = {val:false};
gdjs.TitleCode.condition0IsTrue_0 = {val:false};
gdjs.TitleCode.condition1IsTrue_0 = {val:false};
gdjs.TitleCode.condition2IsTrue_0 = {val:false};


gdjs.TitleCode.eventsList0x6b6314 = function(runtimeScene) {

{


{
gdjs.TitleCode.GDBoxObjects2.createFrom(runtimeScene.getObjects("Box"));
gdjs.TitleCode.GDPlayerNameObjects2.createFrom(runtimeScene.getObjects("PlayerName"));
gdjs.TitleCode.GDPlayerScoreObjects2.createFrom(runtimeScene.getObjects("PlayerScore"));
{runtimeScene.getGame().getVariables().getFromIndex(0).setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")));
}{for(var i = 0, len = gdjs.TitleCode.GDPlayerNameObjects2.length ;i < len;++i) {
    gdjs.TitleCode.GDPlayerNameObjects2[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}
}{for(var i = 0, len = gdjs.TitleCode.GDPlayerScoreObjects2.length ;i < len;++i) {
    gdjs.TitleCode.GDPlayerScoreObjects2[i].setString(gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
}{for(var i = 0, len = gdjs.TitleCode.GDPlayerNameObjects2.length ;i < len;++i) {
    gdjs.TitleCode.GDPlayerNameObjects2[i].putAroundObject((gdjs.TitleCode.GDBoxObjects2.length !== 0 ? gdjs.TitleCode.GDBoxObjects2[0] : null), -(15), 90);
}
}{for(var i = 0, len = gdjs.TitleCode.GDPlayerScoreObjects2.length ;i < len;++i) {
    gdjs.TitleCode.GDPlayerScoreObjects2[i].putAroundObject((gdjs.TitleCode.GDBoxObjects2.length !== 0 ? gdjs.TitleCode.GDBoxObjects2[0] : null), 10, 90);
}
}}

}


{


gdjs.TitleCode.condition0IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")) == "";
}if (gdjs.TitleCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("StoragePlayerName").setString("HUNTER" + gdjs.evtTools.common.toString(gdjs.randomInRange(1000, 9999)));
}{runtimeScene.getGame().getVariables().getFromIndex(0).setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")));
}{gdjs.evtTools.storage.writeStringInJSONFile("KnuckHunt", "PlayerName", gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


{


gdjs.TitleCode.condition0IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")) == "0";
}if (gdjs.TitleCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("StoragePlayerName").setString("HUNTER" + gdjs.evtTools.common.toString(gdjs.randomInRange(1000, 9999)));
}{runtimeScene.getGame().getVariables().getFromIndex(0).setString(gdjs.evtTools.common.getVariableString(runtimeScene.getVariables().get("StoragePlayerName")));
}{gdjs.evtTools.storage.writeStringInJSONFile("KnuckHunt", "PlayerName", gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)));
}}

}


}; //End of gdjs.TitleCode.eventsList0x6b6314
gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDStartGameObjects1Objects = Hashtable.newFrom({"StartGame": gdjs.TitleCode.GDStartGameObjects1});gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDOtherGamesObjects1Objects = Hashtable.newFrom({"OtherGames": gdjs.TitleCode.GDOtherGamesObjects1});gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDHighScoresObjects1Objects = Hashtable.newFrom({"HighScores": gdjs.TitleCode.GDHighScoresObjects1});gdjs.TitleCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.TitleCode.condition0IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.TitleCode.condition0IsTrue_0.val) {
{gdjs.adMob.loadBanner("ca-app-pub-8225116338789252/7014216710", "", true, true, true, false);
}{gdjs.adMob.loadInterstitial("ca-app-pub-8225116338789252/5670836140", "", false, false);
}{gdjs.evtTools.storage.readStringFromJSONFile("KnuckHunt", "PlayerName", runtimeScene, runtimeScene.getVariables().get("StoragePlayerName"));
}{gdjs.evtTools.storage.readNumberFromJSONFile("KnuckHunt", "PlayerScore", runtimeScene, runtimeScene.getVariables().get("StoragePlayerScore"));
}{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("StoragePlayerScore")));
}{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(0);
}
{ //Subevents
gdjs.TitleCode.eventsList0x6b6314(runtimeScene);} //End of subevents
}

}


{


{
gdjs.TitleCode.GDCloudsObjects1.createFrom(runtimeScene.getObjects("Clouds"));
gdjs.TitleCode.GDGrassObjects1.createFrom(runtimeScene.getObjects("Grass"));
{for(var i = 0, len = gdjs.TitleCode.GDCloudsObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDCloudsObjects1[i].setXOffset(gdjs.TitleCode.GDCloudsObjects1[i].getXOffset() + (0.15));
}
}{for(var i = 0, len = gdjs.TitleCode.GDGrassObjects1.length ;i < len;++i) {
    gdjs.TitleCode.GDGrassObjects1[i].setXOffset(gdjs.TitleCode.GDGrassObjects1[i].getXOffset() + (0.05));
}
}}

}


{

gdjs.TitleCode.GDStartGameObjects1.createFrom(runtimeScene.getObjects("StartGame"));

gdjs.TitleCode.condition0IsTrue_0.val = false;
gdjs.TitleCode.condition1IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.TitleCode.condition0IsTrue_0.val ) {
{
gdjs.TitleCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDStartGameObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.TitleCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Level_00", false);
}}

}


{

gdjs.TitleCode.GDOtherGamesObjects1.createFrom(runtimeScene.getObjects("OtherGames"));

gdjs.TitleCode.condition0IsTrue_0.val = false;
gdjs.TitleCode.condition1IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.TitleCode.condition0IsTrue_0.val ) {
{
gdjs.TitleCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDOtherGamesObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.TitleCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "OtherGames", false);
}}

}


{

gdjs.TitleCode.GDHighScoresObjects1.createFrom(runtimeScene.getObjects("HighScores"));

gdjs.TitleCode.condition0IsTrue_0.val = false;
gdjs.TitleCode.condition1IsTrue_0.val = false;
{
gdjs.TitleCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.TitleCode.condition0IsTrue_0.val ) {
{
gdjs.TitleCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.TitleCode.mapOfGDgdjs_46TitleCode_46GDHighScoresObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.TitleCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "HighScores", false);
}}

}


}; //End of gdjs.TitleCode.eventsList0x5b7028


gdjs.TitleCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.TitleCode.GDGrassObjects1.length = 0;
gdjs.TitleCode.GDGrassObjects2.length = 0;
gdjs.TitleCode.GDGrassObjects3.length = 0;
gdjs.TitleCode.GDTreesObjects1.length = 0;
gdjs.TitleCode.GDTreesObjects2.length = 0;
gdjs.TitleCode.GDTreesObjects3.length = 0;
gdjs.TitleCode.GDSkyObjects1.length = 0;
gdjs.TitleCode.GDSkyObjects2.length = 0;
gdjs.TitleCode.GDSkyObjects3.length = 0;
gdjs.TitleCode.GDCloudsObjects1.length = 0;
gdjs.TitleCode.GDCloudsObjects2.length = 0;
gdjs.TitleCode.GDCloudsObjects3.length = 0;
gdjs.TitleCode.GDBirdObjects1.length = 0;
gdjs.TitleCode.GDBirdObjects2.length = 0;
gdjs.TitleCode.GDBirdObjects3.length = 0;
gdjs.TitleCode.GDDogMissedObjects1.length = 0;
gdjs.TitleCode.GDDogMissedObjects2.length = 0;
gdjs.TitleCode.GDDogMissedObjects3.length = 0;
gdjs.TitleCode.GDKnuckHuntObjects1.length = 0;
gdjs.TitleCode.GDKnuckHuntObjects2.length = 0;
gdjs.TitleCode.GDKnuckHuntObjects3.length = 0;
gdjs.TitleCode.GDStartGameObjects1.length = 0;
gdjs.TitleCode.GDStartGameObjects2.length = 0;
gdjs.TitleCode.GDStartGameObjects3.length = 0;
gdjs.TitleCode.GDHighScoresObjects1.length = 0;
gdjs.TitleCode.GDHighScoresObjects2.length = 0;
gdjs.TitleCode.GDHighScoresObjects3.length = 0;
gdjs.TitleCode.GDBoxObjects1.length = 0;
gdjs.TitleCode.GDBoxObjects2.length = 0;
gdjs.TitleCode.GDBoxObjects3.length = 0;
gdjs.TitleCode.GDOtherGamesObjects1.length = 0;
gdjs.TitleCode.GDOtherGamesObjects2.length = 0;
gdjs.TitleCode.GDOtherGamesObjects3.length = 0;
gdjs.TitleCode.GDPlayerScoreObjects1.length = 0;
gdjs.TitleCode.GDPlayerScoreObjects2.length = 0;
gdjs.TitleCode.GDPlayerScoreObjects3.length = 0;
gdjs.TitleCode.GDPlayerNameObjects1.length = 0;
gdjs.TitleCode.GDPlayerNameObjects2.length = 0;
gdjs.TitleCode.GDPlayerNameObjects3.length = 0;

gdjs.TitleCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['TitleCode'] = gdjs.TitleCode;
