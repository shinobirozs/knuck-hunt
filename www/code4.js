gdjs.Level_9500Code = {};
gdjs.Level_9500Code.GDGrassObjects1= [];
gdjs.Level_9500Code.GDGrassObjects2= [];
gdjs.Level_9500Code.GDGrassObjects3= [];
gdjs.Level_9500Code.GDTreesObjects1= [];
gdjs.Level_9500Code.GDTreesObjects2= [];
gdjs.Level_9500Code.GDTreesObjects3= [];
gdjs.Level_9500Code.GDSkyObjects1= [];
gdjs.Level_9500Code.GDSkyObjects2= [];
gdjs.Level_9500Code.GDSkyObjects3= [];
gdjs.Level_9500Code.GDCloudsObjects1= [];
gdjs.Level_9500Code.GDCloudsObjects2= [];
gdjs.Level_9500Code.GDCloudsObjects3= [];
gdjs.Level_9500Code.GDBirdObjects1= [];
gdjs.Level_9500Code.GDBirdObjects2= [];
gdjs.Level_9500Code.GDBirdObjects3= [];
gdjs.Level_9500Code.GDDogMissedObjects1= [];
gdjs.Level_9500Code.GDDogMissedObjects2= [];
gdjs.Level_9500Code.GDDogMissedObjects3= [];
gdjs.Level_9500Code.GDTapToStartObjects1= [];
gdjs.Level_9500Code.GDTapToStartObjects2= [];
gdjs.Level_9500Code.GDTapToStartObjects3= [];
gdjs.Level_9500Code.GDJumperObjects1= [];
gdjs.Level_9500Code.GDJumperObjects2= [];
gdjs.Level_9500Code.GDJumperObjects3= [];
gdjs.Level_9500Code.GDDestroyerObjects1= [];
gdjs.Level_9500Code.GDDestroyerObjects2= [];
gdjs.Level_9500Code.GDDestroyerObjects3= [];
gdjs.Level_9500Code.GDBoomObjects1= [];
gdjs.Level_9500Code.GDBoomObjects2= [];
gdjs.Level_9500Code.GDBoomObjects3= [];
gdjs.Level_9500Code.GDMissedObjects1= [];
gdjs.Level_9500Code.GDMissedObjects2= [];
gdjs.Level_9500Code.GDMissedObjects3= [];
gdjs.Level_9500Code.GDLifeObjects1= [];
gdjs.Level_9500Code.GDLifeObjects2= [];
gdjs.Level_9500Code.GDLifeObjects3= [];
gdjs.Level_9500Code.GDBoxObjects1= [];
gdjs.Level_9500Code.GDBoxObjects2= [];
gdjs.Level_9500Code.GDBoxObjects3= [];
gdjs.Level_9500Code.GDScoreObjects1= [];
gdjs.Level_9500Code.GDScoreObjects2= [];
gdjs.Level_9500Code.GDScoreObjects3= [];
gdjs.Level_9500Code.GDInstructionsObjects1= [];
gdjs.Level_9500Code.GDInstructionsObjects2= [];
gdjs.Level_9500Code.GDInstructionsObjects3= [];

gdjs.Level_9500Code.conditionTrue_0 = {val:false};
gdjs.Level_9500Code.condition0IsTrue_0 = {val:false};
gdjs.Level_9500Code.condition1IsTrue_0 = {val:false};
gdjs.Level_9500Code.condition2IsTrue_0 = {val:false};
gdjs.Level_9500Code.condition3IsTrue_0 = {val:false};
gdjs.Level_9500Code.condition4IsTrue_0 = {val:false};
gdjs.Level_9500Code.conditionTrue_1 = {val:false};
gdjs.Level_9500Code.condition0IsTrue_1 = {val:false};
gdjs.Level_9500Code.condition1IsTrue_1 = {val:false};
gdjs.Level_9500Code.condition2IsTrue_1 = {val:false};
gdjs.Level_9500Code.condition3IsTrue_1 = {val:false};
gdjs.Level_9500Code.condition4IsTrue_1 = {val:false};


gdjs.Level_9500Code.eventsList0x6b896c = function(runtimeScene) {

{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) >= 0;
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
gdjs.Level_9500Code.GDLifeObjects1.createFrom(runtimeScene.getObjects("Life"));
gdjs.Level_9500Code.GDScoreObjects1.createFrom(runtimeScene.getObjects("Score"));
{for(var i = 0, len = gdjs.Level_9500Code.GDLifeObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDLifeObjects1[i].setWidth(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) * 34);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDScoreObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDScoreObjects1[i].setString("X" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(3)));
}
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6b896c
gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects2Objects = Hashtable.newFrom({"Bird": gdjs.Level_9500Code.GDBirdObjects2});gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBoomObjects2ObjectsGDgdjs_46Level_959500Code_46GDMissedObjects2Objects = Hashtable.newFrom({"Boom": gdjs.Level_9500Code.GDBoomObjects2, "Missed": gdjs.Level_9500Code.GDMissedObjects2});gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects = Hashtable.newFrom({"Bird": gdjs.Level_9500Code.GDBirdObjects1});gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBoomObjects1ObjectsGDgdjs_46Level_959500Code_46GDMissedObjects1Objects = Hashtable.newFrom({"Boom": gdjs.Level_9500Code.GDBoomObjects1, "Missed": gdjs.Level_9500Code.GDMissedObjects1});gdjs.Level_9500Code.eventsList0x6b6a7c = function(runtimeScene) {

{

gdjs.Level_9500Code.GDBirdObjects2.createFrom(runtimeScene.getObjects("Bird"));

gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects2Objects, runtimeScene, true, true);
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
gdjs.Level_9500Code.GDBoomObjects2.length = 0;

gdjs.Level_9500Code.GDMissedObjects2.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBoomObjects2ObjectsGDgdjs_46Level_959500Code_46GDMissedObjects2Objects, "Missed", gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), "");
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects2[i].setScale(0.5);
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects2[i].setScale(0.5);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects2[i].setZOrder(10);
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects2[i].setZOrder(10);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects2[i].playAnimation();
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects2.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects2[i].playAnimation();
}
}}

}


{

gdjs.Level_9500Code.GDBirdObjects1.createFrom(runtimeScene.getObjects("Bird"));

gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects, runtimeScene, true, false);
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Level_9500Code.GDBirdObjects1 */
gdjs.Level_9500Code.GDBoomObjects1.length = 0;

gdjs.Level_9500Code.GDMissedObjects1.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBoomObjects1ObjectsGDgdjs_46Level_959500Code_46GDMissedObjects1Objects, "Boom", gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), "");
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects1[i].setZOrder(10);
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects1[i].setZOrder(10);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects1[i].playAnimation();
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects1[i].playAnimation();
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(3).add(1);
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6b6a7c
gdjs.Level_9500Code.eventsList0x6bf054 = function(runtimeScene) {

{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
{gdjs.Level_9500Code.conditionTrue_1 = gdjs.Level_9500Code.condition0IsTrue_0;
gdjs.Level_9500Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7046940);
}
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/reload.mp3", false, 100, 1);
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6bf054
gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects = Hashtable.newFrom({"Bird": gdjs.Level_9500Code.GDBirdObjects1});gdjs.Level_9500Code.eventsList0x6bed0c = function(runtimeScene) {

{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("NextSpawnSound")) <= 3;
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/bounce-2.mp3", false, 100, 1);
}}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
gdjs.Level_9500Code.condition1IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("NextSpawnSound")) >= 4;
}if ( gdjs.Level_9500Code.condition0IsTrue_0.val ) {
{
gdjs.Level_9500Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("NextSpawnSound")) <= 6;
}}
if (gdjs.Level_9500Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/bounce.mp3", false, 100, 1);
}}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("NextSpawnSound")) >= 7;
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/cluck.mp3", false, 100, 1);
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6bed0c
gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects = Hashtable.newFrom({"Bird": gdjs.Level_9500Code.GDBirdObjects1});gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDDestroyerObjects1Objects = Hashtable.newFrom({"Destroyer": gdjs.Level_9500Code.GDDestroyerObjects1});gdjs.Level_9500Code.userFunc0x6d1f20 = function(runtimeScene) {
"use strict";
var myGlobalVars = runtimeScene.getGame().getVariables();
var playerName = myGlobalVars.get("PlayerName").getAsString();
var playerScore = myGlobalVars.get("PlayerScore").getAsString();
var secretKey = myGlobalVars.get("SecretKey").getAsString();

var MD5 = function (string) {

   function RotateLeft(lValue, iShiftBits) {
       return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
   }


   function AddUnsigned(lX, lY) {
       var lX4, lY4, lX8, lY8, lResult;
       lX8 = (lX & 0x80000000);
       lY8 = (lY & 0x80000000);
       lX4 = (lX & 0x40000000);
       lY4 = (lY & 0x40000000);
       lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
       if (lX4 & lY4) {
           return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
       }
       if (lX4 | lY4) {
           if (lResult & 0x40000000) {
               return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
           } else {
               return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
           }
       } else {
           return (lResult ^ lX8 ^ lY8);
       }
   }


   function F(x, y, z) { return (x & y) | ((~x) & z); }
   function G(x, y, z) { return (x & z) | (y & (~z)); }
   function H(x, y, z) { return (x ^ y ^ z); }
   function I(x, y, z) { return (y ^ (x | (~z))); }


   function FF(a, b, c, d, x, s, ac) {
       a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
       return AddUnsigned(RotateLeft(a, s), b);
   };


   function GG(a, b, c, d, x, s, ac) {
       a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
       return AddUnsigned(RotateLeft(a, s), b);
   };


   function HH(a, b, c, d, x, s, ac) {
       a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
       return AddUnsigned(RotateLeft(a, s), b);
   };


   function II(a, b, c, d, x, s, ac) {
       a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
       return AddUnsigned(RotateLeft(a, s), b);
   };


   function ConvertToWordArray(string) {
       var lWordCount;
       var lMessageLength = string.length;
       var lNumberOfWords_temp1 = lMessageLength + 8;
       var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
       var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
       var lWordArray = Array(lNumberOfWords - 1);
       var lBytePosition = 0;
       var lByteCount = 0;
       while (lByteCount < lMessageLength) {
           lWordCount = (lByteCount - (lByteCount % 4)) / 4;
           lBytePosition = (lByteCount % 4) * 8;
           lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount) << lBytePosition));
           lByteCount++;
       }
       lWordCount = (lByteCount - (lByteCount % 4)) / 4;
       lBytePosition = (lByteCount % 4) * 8;
       lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
       lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
       lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
       return lWordArray;
   };


   function WordToHex(lValue) {
       var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
       for (lCount = 0; lCount <= 3; lCount++) {
           lByte = (lValue >>> (lCount * 8)) & 255;
           WordToHexValue_temp = "0" + lByte.toString(16);
           WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
       }
       return WordToHexValue;
   };


   function Utf8Encode(string) {
       string = string.replace(/\r\n/g, "\n");
       var utftext = "";


       for (var n = 0; n < string.length; n++) {


           var c = string.charCodeAt(n);


           if (c < 128) {
               utftext += String.fromCharCode(c);
           }
           else if ((c > 127) && (c < 2048)) {
               utftext += String.fromCharCode((c >> 6) | 192);
               utftext += String.fromCharCode((c & 63) | 128);
           }
           else {
               utftext += String.fromCharCode((c >> 12) | 224);
               utftext += String.fromCharCode(((c >> 6) & 63) | 128);
               utftext += String.fromCharCode((c & 63) | 128);
           }


       }


       return utftext;
   };


   var x = Array();
   var k, AA, BB, CC, DD, a, b, c, d;
   var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
   var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
   var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
   var S41 = 6, S42 = 10, S43 = 15, S44 = 21;


   string = Utf8Encode(string);


   x = ConvertToWordArray(string);


   a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;


   for (k = 0; k < x.length; k += 16) {
       AA = a; BB = b; CC = c; DD = d;
       a = FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
       d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
       c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
       b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
       a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
       d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
       c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
       b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
       a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
       d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
       c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
       b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
       a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
       d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
       c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
       b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
       a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
       d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
       c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
       b = GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
       a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
       d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
       c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
       b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
       a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
       d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
       c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
       b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
       a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
       d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
       c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
       b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
       a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
       d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
       c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
       b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
       a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
       d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
       c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
       b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
       a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
       d = HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
       c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
       b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
       a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
       d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
       c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
       b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
       a = II(a, b, c, d, x[k + 0], S41, 0xF4292244);
       d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
       c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
       b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
       a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
       d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
       c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
       b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
       a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
       d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
       c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
       b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
       a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
       d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
       c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
       b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
       a = AddUnsigned(a, AA);
       b = AddUnsigned(b, BB);
       c = AddUnsigned(c, CC);
       d = AddUnsigned(d, DD);
   }


   var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);


   return temp.toLowerCase();

}

var hash = MD5(playerName + playerScore + secretKey);
myGlobalVars.get("HashKey").setString(hash);

};
gdjs.Level_9500Code.eventsList0x6d105c = function(runtimeScene) {

{


gdjs.Level_9500Code.userFunc0x6d1f20(runtimeScene);

}


{


{
{gdjs.evtTools.network.sendHttpRequest("https://hacktomatest.com", "/AddScoreKnuckHunt.php?name=" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(0)) + "&score=" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().getFromIndex(1)) + "&hash=" + gdjs.evtTools.common.getVariableString(runtimeScene.getGame().getVariables().get("HashKey")), "", "GET", "", runtimeScene.getVariables().get("ResponseAddScore"));
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6d105c
gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects = Hashtable.newFrom({"Bird": gdjs.Level_9500Code.GDBirdObjects1});gdjs.Level_9500Code.eventsList0x6d0d2c = function(runtimeScene) {

{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) > gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1));
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)));
}{gdjs.evtTools.storage.writeNumberInJSONFile("KnuckHunt", "PlayerScore", gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)));
}
{ //Subevents
gdjs.Level_9500Code.eventsList0x6d105c(runtimeScene);} //End of subevents
}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
{gdjs.Level_9500Code.conditionTrue_1 = gdjs.Level_9500Code.condition0IsTrue_0;
gdjs.Level_9500Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7152396);
}
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/escape.mp3", false, 100, 1);
}}

}


{

/* Reuse gdjs.Level_9500Code.GDBirdObjects1 */

gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects) <= 0;
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Title", true);
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x6d0d2c
gdjs.Level_9500Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
gdjs.Level_9500Code.GDTapToStartObjects1.createFrom(runtimeScene.getObjects("TapToStart"));
{gdjs.adMob.showInterstitial();
}{for(var i = 0, len = gdjs.Level_9500Code.GDTapToStartObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDTapToStartObjects1[i].getBehavior("Flash").Flash(120, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "BirdSpawner");
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "BirdSpawner");
}{for(var i = 0, len = gdjs.Level_9500Code.GDTapToStartObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDTapToStartObjects1[i].setOpacity(100);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "CanShoot");
}{runtimeScene.getVariables().getFromIndex(3).setNumber(1);
}}

}


{


{
gdjs.Level_9500Code.GDCloudsObjects1.createFrom(runtimeScene.getObjects("Clouds"));
gdjs.Level_9500Code.GDGrassObjects1.createFrom(runtimeScene.getObjects("Grass"));
gdjs.Level_9500Code.GDTreesObjects1.createFrom(runtimeScene.getObjects("Trees"));
{for(var i = 0, len = gdjs.Level_9500Code.GDCloudsObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDCloudsObjects1[i].setXOffset(gdjs.Level_9500Code.GDCloudsObjects1[i].getXOffset() + (0.15));
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDTreesObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDTreesObjects1[i].setXOffset(gdjs.Level_9500Code.GDTreesObjects1[i].getXOffset() + (0.04));
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDGrassObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDGrassObjects1[i].setXOffset(gdjs.Level_9500Code.GDGrassObjects1[i].getXOffset() + (0.05));
}
}
{ //Subevents
gdjs.Level_9500Code.eventsList0x6b896c(runtimeScene);} //End of subevents
}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
gdjs.Level_9500Code.condition1IsTrue_0.val = false;
gdjs.Level_9500Code.condition2IsTrue_0.val = false;
gdjs.Level_9500Code.condition3IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.Level_9500Code.condition0IsTrue_0.val ) {
{
gdjs.Level_9500Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) >= 0;
}if ( gdjs.Level_9500Code.condition1IsTrue_0.val ) {
{
gdjs.Level_9500Code.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(3)) == 1;
}if ( gdjs.Level_9500Code.condition2IsTrue_0.val ) {
{
gdjs.Level_9500Code.condition3IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 1;
}}
}
}
if (gdjs.Level_9500Code.condition3IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/shot.mp3", false, 100, 1);
}{runtimeScene.getVariables().getFromIndex(3).setNumber(0);
}
{ //Subevents
gdjs.Level_9500Code.eventsList0x6b6a7c(runtimeScene);} //End of subevents
}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {

{ //Subevents
gdjs.Level_9500Code.eventsList0x6bf054(runtimeScene);} //End of subevents
}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
gdjs.Level_9500Code.condition1IsTrue_0.val = false;
gdjs.Level_9500Code.condition2IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.Level_9500Code.condition0IsTrue_0.val ) {
{
gdjs.Level_9500Code.condition1IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}if ( gdjs.Level_9500Code.condition1IsTrue_0.val ) {
{
{gdjs.Level_9500Code.conditionTrue_1 = gdjs.Level_9500Code.condition2IsTrue_0;
gdjs.Level_9500Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7071212);
}
}}
}
if (gdjs.Level_9500Code.condition2IsTrue_0.val) {
gdjs.Level_9500Code.GDBoxObjects1.createFrom(runtimeScene.getObjects("Box"));
gdjs.Level_9500Code.GDDogMissedObjects1.createFrom(runtimeScene.getObjects("DogMissed"));
gdjs.Level_9500Code.GDInstructionsObjects1.createFrom(runtimeScene.getObjects("Instructions"));
gdjs.Level_9500Code.GDTapToStartObjects1.createFrom(runtimeScene.getObjects("TapToStart"));
{for(var i = 0, len = gdjs.Level_9500Code.GDTapToStartObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDTapToStartObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBoxObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoxObjects1[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDInstructionsObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDInstructionsObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "BirdSpawner");
}{for(var i = 0, len = gdjs.Level_9500Code.GDDogMissedObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDDogMissedObjects1[i].hide();
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "assets/sounds/intro.mp3", false, 100, 1);
}}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.75, "BirdSpawner");
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
gdjs.Level_9500Code.GDBirdObjects1.length = 0;

{gdjs.evtTools.object.createObjectFromGroupOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects, "Bird", -(100), gdjs.randomInRange(10, 350), "");
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].setScale(0.5);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].setZOrder(10);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "BirdSpawner");
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].getBehavior("PlatformerObject").setGravity(gdjs.randomInRange(200, 1000));
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].getBehavior("PlatformerObject").setAcceleration(gdjs.randomInRange(200, 1500));
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].setAnimation(gdjs.randomInRange(0, 3));
}
}{runtimeScene.getVariables().get("NextSpawnSound").setNumber(gdjs.randomInRange(1, 10));
}
{ //Subevents
gdjs.Level_9500Code.eventsList0x6bed0c(runtimeScene);} //End of subevents
}

}


{


{
gdjs.Level_9500Code.GDBirdObjects1.createFrom(runtimeScene.getObjects("Bird"));
{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].rotate(10, runtimeScene);
}
}}

}


{

gdjs.Level_9500Code.GDBirdObjects1.createFrom(runtimeScene.getObjects("Bird"));
gdjs.Level_9500Code.GDDestroyerObjects1.createFrom(runtimeScene.getObjects("Destroyer"));

gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDBirdObjects1Objects, gdjs.Level_9500Code.mapOfGDgdjs_46Level_959500Code_46GDDestroyerObjects1Objects, false, runtimeScene, false);
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Level_9500Code.GDBirdObjects1 */
{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getVariables().getFromIndex(1).sub(1);
}}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(1)) <= 0;
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
gdjs.Level_9500Code.GDBirdObjects1.createFrom(runtimeScene.getObjects("Bird"));
gdjs.Level_9500Code.GDDogMissedObjects1.createFrom(runtimeScene.getObjects("DogMissed"));
{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "BirdSpawner");
}{for(var i = 0, len = gdjs.Level_9500Code.GDBirdObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBirdObjects1[i].getBehavior("PlatformerObject").setGravity(5000);
}
}{for(var i = 0, len = gdjs.Level_9500Code.GDDogMissedObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDDogMissedObjects1[i].hide(false);
}
}
{ //Subevents
gdjs.Level_9500Code.eventsList0x6d0d2c(runtimeScene);} //End of subevents
}

}


{

gdjs.Level_9500Code.GDBoomObjects1.createFrom(runtimeScene.getObjects("Boom"));
gdjs.Level_9500Code.GDMissedObjects1.createFrom(runtimeScene.getObjects("Missed"));

gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.Level_9500Code.GDBoomObjects1.length;i<l;++i) {
    if ( gdjs.Level_9500Code.GDBoomObjects1[i].hasAnimationEnded() ) {
        gdjs.Level_9500Code.condition0IsTrue_0.val = true;
        gdjs.Level_9500Code.GDBoomObjects1[k] = gdjs.Level_9500Code.GDBoomObjects1[i];
        ++k;
    }
}
gdjs.Level_9500Code.GDBoomObjects1.length = k;for(var i = 0, k = 0, l = gdjs.Level_9500Code.GDMissedObjects1.length;i<l;++i) {
    if ( gdjs.Level_9500Code.GDMissedObjects1[i].hasAnimationEnded() ) {
        gdjs.Level_9500Code.condition0IsTrue_0.val = true;
        gdjs.Level_9500Code.GDMissedObjects1[k] = gdjs.Level_9500Code.GDMissedObjects1[i];
        ++k;
    }
}
gdjs.Level_9500Code.GDMissedObjects1.length = k;}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Level_9500Code.GDBoomObjects1 */
/* Reuse gdjs.Level_9500Code.GDMissedObjects1 */
{for(var i = 0, len = gdjs.Level_9500Code.GDBoomObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDBoomObjects1[i].deleteFromScene(runtimeScene);
}
for(var i = 0, len = gdjs.Level_9500Code.GDMissedObjects1.length ;i < len;++i) {
    gdjs.Level_9500Code.GDMissedObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{


gdjs.Level_9500Code.condition0IsTrue_0.val = false;
{
gdjs.Level_9500Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0.5, "CanShoot");
}if (gdjs.Level_9500Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "CanShoot");
}{runtimeScene.getVariables().getFromIndex(3).setNumber(1);
}}

}


}; //End of gdjs.Level_9500Code.eventsList0x5b7028


gdjs.Level_9500Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Level_9500Code.GDGrassObjects1.length = 0;
gdjs.Level_9500Code.GDGrassObjects2.length = 0;
gdjs.Level_9500Code.GDGrassObjects3.length = 0;
gdjs.Level_9500Code.GDTreesObjects1.length = 0;
gdjs.Level_9500Code.GDTreesObjects2.length = 0;
gdjs.Level_9500Code.GDTreesObjects3.length = 0;
gdjs.Level_9500Code.GDSkyObjects1.length = 0;
gdjs.Level_9500Code.GDSkyObjects2.length = 0;
gdjs.Level_9500Code.GDSkyObjects3.length = 0;
gdjs.Level_9500Code.GDCloudsObjects1.length = 0;
gdjs.Level_9500Code.GDCloudsObjects2.length = 0;
gdjs.Level_9500Code.GDCloudsObjects3.length = 0;
gdjs.Level_9500Code.GDBirdObjects1.length = 0;
gdjs.Level_9500Code.GDBirdObjects2.length = 0;
gdjs.Level_9500Code.GDBirdObjects3.length = 0;
gdjs.Level_9500Code.GDDogMissedObjects1.length = 0;
gdjs.Level_9500Code.GDDogMissedObjects2.length = 0;
gdjs.Level_9500Code.GDDogMissedObjects3.length = 0;
gdjs.Level_9500Code.GDTapToStartObjects1.length = 0;
gdjs.Level_9500Code.GDTapToStartObjects2.length = 0;
gdjs.Level_9500Code.GDTapToStartObjects3.length = 0;
gdjs.Level_9500Code.GDJumperObjects1.length = 0;
gdjs.Level_9500Code.GDJumperObjects2.length = 0;
gdjs.Level_9500Code.GDJumperObjects3.length = 0;
gdjs.Level_9500Code.GDDestroyerObjects1.length = 0;
gdjs.Level_9500Code.GDDestroyerObjects2.length = 0;
gdjs.Level_9500Code.GDDestroyerObjects3.length = 0;
gdjs.Level_9500Code.GDBoomObjects1.length = 0;
gdjs.Level_9500Code.GDBoomObjects2.length = 0;
gdjs.Level_9500Code.GDBoomObjects3.length = 0;
gdjs.Level_9500Code.GDMissedObjects1.length = 0;
gdjs.Level_9500Code.GDMissedObjects2.length = 0;
gdjs.Level_9500Code.GDMissedObjects3.length = 0;
gdjs.Level_9500Code.GDLifeObjects1.length = 0;
gdjs.Level_9500Code.GDLifeObjects2.length = 0;
gdjs.Level_9500Code.GDLifeObjects3.length = 0;
gdjs.Level_9500Code.GDBoxObjects1.length = 0;
gdjs.Level_9500Code.GDBoxObjects2.length = 0;
gdjs.Level_9500Code.GDBoxObjects3.length = 0;
gdjs.Level_9500Code.GDScoreObjects1.length = 0;
gdjs.Level_9500Code.GDScoreObjects2.length = 0;
gdjs.Level_9500Code.GDScoreObjects3.length = 0;
gdjs.Level_9500Code.GDInstructionsObjects1.length = 0;
gdjs.Level_9500Code.GDInstructionsObjects2.length = 0;
gdjs.Level_9500Code.GDInstructionsObjects3.length = 0;

gdjs.Level_9500Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['Level_9500Code'] = gdjs.Level_9500Code;
